﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    // Start is called before the first frame update
    public int Healths;
    public GameObject Nyawa1, Nyawa2, Nyawa3, Kalah;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Healths == 3)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(true);
            Nyawa3.SetActive(true);
        }
        else if (Healths == 2)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(true);
            Nyawa3.SetActive(false);
        }
        else if (Healths == 1)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(false);
            Nyawa3.SetActive(false);
        }
        else if (Healths == 0)
        {
            Nyawa1.SetActive(false);
            Nyawa2.SetActive(false);
            Nyawa3.SetActive(false);
            Kalah.SetActive(true);
            print("Habis");

        }
    }
}
