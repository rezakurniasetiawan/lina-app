﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public void Mulai()
    {
        SceneManager.LoadScene("Mulai");
    }
    public void BacktoMain()
    {
        SceneManager.LoadScene("MainScreen");
    }
    public void Level1()
    {
        SceneManager.LoadScene("Level1");
    }
    public void Level1Play()
    {
        SceneManager.LoadScene("Level1_play");
    }
    public void Level1_1Play()
    {
        SceneManager.LoadScene("Level1-1_play");
    }
     public void Level2Play()
    {
        SceneManager.LoadScene("Level2_play");
    }
    public void Level2_2Play()
    {
        SceneManager.LoadScene("Level2-2_play");
    }
    public void Level3Play()
    {
        SceneManager.LoadScene("Level3_Play");
    }
}
